package com.uk.creditdecision.resttemplate;

import java.util.Optional;


import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CreditScoreAPI {

	
    private RestTemplate restTemplate;

	public CreditScoreAPI(RestTemplateBuilder builder) {
		this.restTemplate = builder.build();
	}

	/**
	 * This method is used to call an external service to get the credit score of the ssn requested.
	 * @param ssn
	 * @return Long
	 */
	public Optional<Long> getCreditScore(String ssn) {

		// call the external for credit score;
		/*
		 * HttpEntity<String> entity = new HttpEntity<>(ssn, headers);
		 * ResponseEntity<Long> responseEntity =
		 * restTemplate.exchange("url to be called", HttpMethod.GET, entity,
		 * Long.class);
		 */
		
		ResponseEntity<Long> responseEntity = new ResponseEntity<>(Long.valueOf("702"), HttpStatus.OK);
		/*
		 * Mockito.when(restTemplate.exchange(Matchers.anyString(),
		 * Matchers.any(HttpMethod.class), Matchers.<HttpEntity<?>>any(),
		 * Matchers.<Class<Long>>any())).thenReturn(responseEntity);
		 */
/*
		Mockito.when(restTemplate.getForEntity("http://localhost:8080/creditscore/ssn", 
				Long.class)).thenReturn(responseEntity);*/

		return Optional.ofNullable(responseEntity.getBody());

	}
}
