
package com.uk.creditdecision.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class CustomExceptionResponse {

	private String errorCode;

	private HttpStatus status;

	private String message;

	public CustomExceptionResponse(String errorCode, HttpStatus status, String message) {
		this.errorCode = errorCode;
		this.status = status;
		this.message = message;
	}

	@Override
	public String toString() {
		return "CustomExceptionResponse{" + "errorCode=" + errorCode + ", status='" + status + '\'' + ", message='"
				+ message + '\'' + '}';
	}
}
