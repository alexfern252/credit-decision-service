/**
 * 
 */
package com.uk.creditdecision.service;

import java.math.BigDecimal;

import com.uk.creditdecision.model.response.CreditDecisionResponse;

/**
 * @author Alexandre Fernandes
 *
 */
public interface CreditDecisionService {

	CreditDecisionResponse sanctionLoanAmount(String ssn, BigDecimal loanAmount, BigDecimal annualIncome);

}
