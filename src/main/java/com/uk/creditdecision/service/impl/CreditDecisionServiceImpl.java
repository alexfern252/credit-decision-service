/**
 * 
 */
package com.uk.creditdecision.service.impl;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uk.creditdecision.model.response.CreditDecisionResponse;
import com.uk.creditdecision.resttemplate.CreditScoreAPI;
import com.uk.creditdecision.service.CreditDecisionService;

/**
 * @author Alexandre Fernandes
 *
 */
@Service
public class CreditDecisionServiceImpl implements CreditDecisionService {

	@Autowired
	private CreditScoreAPI creditScoreAPI;

	@Override
	public CreditDecisionResponse sanctionLoanAmount(String ssn, BigDecimal loanAmount, BigDecimal annualIncome) {

		BigDecimal returnVal = new BigDecimal(0);
		CreditDecisionResponse resp = new CreditDecisionResponse();
		Optional<Long> opt = creditScoreAPI.getCreditScore(ssn);
		if (opt.isPresent()) {
			if (opt.get() > 700) {
				returnVal = calculateLoan(annualIncome);
				resp.setLoanApproved("Y");
			}
		}
		resp.setSanctionedLoanAmount(returnVal);
		resp.setSsn(ssn);
		return resp;
	}

	/**
	 * This method calculates the loan amount based on the incoming annual income.
	 * 
	 * @param annualIncome
	 * @return BigDecimal
	 */
	private BigDecimal calculateLoan(BigDecimal annualIncome) {
		return annualIncome.divide(new BigDecimal(2));
	}

}
