/**
 * 
 */
package com.uk.creditdecision.model.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Alexandre Fernandes
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreditDecisionResponse {

	@JsonProperty(value = "ssn")
	private String ssn;

	@JsonProperty(value = "loanApproved")
	private String loanApproved ="N";
	
	@JsonProperty(value = "sanctionedLoanAmount")
	private BigDecimal sanctionedLoanAmount;

}
