/**
 * 
 */
package com.uk.creditdecision.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uk.creditdecision.exception.AppException;
import com.uk.creditdecision.model.response.CreditDecisionResponse;
import com.uk.creditdecision.service.CreditDecisionService;

import lombok.NonNull;

/**
 * @author Alexandre Fernandes
 *
 */
@RestController
@RequestMapping(path = "/api/v1")
@Validated
public class CreditDecisonController {

	@Autowired
	private CreditDecisionService decisionService; 

	@GetMapping(path = "/sanction-loan/{ssn}/{loanAmount}/{annualIncome}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreditDecisionResponse> sanctionLoanAmount(@PathVariable("ssn") @NonNull String ssn,
			@PathVariable("loanAmount") @NonNull BigDecimal loanAmount,
			@PathVariable("annualIncome") @NonNull BigDecimal annualIncome) throws AppException {

		CreditDecisionResponse resp = decisionService.sanctionLoanAmount(ssn, loanAmount, annualIncome);
		return new ResponseEntity<>(resp, HttpStatus.OK);

	}
}
