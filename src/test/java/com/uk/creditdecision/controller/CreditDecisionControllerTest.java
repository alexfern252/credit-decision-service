package com.uk.creditdecision.controller;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.uk.creditdecision.model.response.CreditDecisionResponse;
import com.uk.creditdecision.service.impl.CreditDecisionServiceImpl;

/**
 * @author Alexandre Fernandes 
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CreditDecisionControllerTest { 

	@Autowired
	private MockMvc mockMvc;

	@Mock
	private CreditDecisionServiceImpl creditDecisionService;

	@InjectMocks
	private CreditDecisonController creditDecisonController;

	private String ssn = null;
	private BigDecimal loanAmount = BigDecimal.valueOf(0);
	private BigDecimal annualIncome = BigDecimal.valueOf(0);
	
 

	@Before
	public void setUp()  {
		
		// Initializes the JacksonTester
		ObjectMapper om = new ObjectMapper();
		om.findAndRegisterModules(); 
		om.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		JacksonTester.initFields(this, om);
		// MockMvc standalone approach
		ssn = "23784";
		loanAmount = new BigDecimal(50000);
		annualIncome = new BigDecimal(70000);
		mockMvc = MockMvcBuilders.standaloneSetup(creditDecisonController).build();
	}

	@Test
	public void testSanctionLoanAmount_Accepted() throws Exception {

		CreditDecisionResponse response = new CreditDecisionResponse(ssn, "Y", new BigDecimal(45000));

		when(creditDecisionService.sanctionLoanAmount(ssn, loanAmount, annualIncome)).thenReturn(response);

		this.mockMvc
		.perform(get("/api/v1/sanction-loan/{ssn}/{loanAmount}/{annualIncome}", ssn, loanAmount, annualIncome).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().is(200));

		
	}
	
	@Test
	public void testSanctionLoanAmount_Rejected() throws Exception {

		CreditDecisionResponse response = new CreditDecisionResponse(ssn, "N", new BigDecimal(45000));

		when(creditDecisionService.sanctionLoanAmount(ssn, loanAmount, annualIncome)).thenReturn(response);

		MockHttpServletResponse responseOutput = mockMvc
				.perform(get("/api/v1/sanction-loan/{ssn}/{loanAmount}/{annualIncome}", ssn, loanAmount, annualIncome).contentType(MediaType.APPLICATION_JSON_VALUE))
				.andReturn().getResponse();

		assertThat(responseOutput.getStatus(), is(200));
	} 

}
