
package com.uk.creditdecision.service;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.json.JacksonTester;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.uk.creditdecision.model.response.CreditDecisionResponse;
import com.uk.creditdecision.resttemplate.CreditScoreAPI;
import com.uk.creditdecision.service.impl.CreditDecisionServiceImpl;

/**
 * @author E1203174
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CreditDecisionServiceTest {

	@Mock
	private CreditScoreAPI creditScoreAPI;

	@InjectMocks
	private CreditDecisionServiceImpl creditDecisionService;

	private String ssn = null;
	private BigDecimal loanAmt = BigDecimal.valueOf(0);
	private BigDecimal annualIncome = BigDecimal.valueOf(0);

	@Before
	public void setUp() {
		// Initializes the JacksonTester
		ObjectMapper om = new ObjectMapper();
		om.findAndRegisterModules();
		om.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		JacksonTester.initFields(this, om);
		// MockMvc standalone approach
		ssn = "23784";
		loanAmt = new BigDecimal(50000);
		annualIncome = new BigDecimal(70000);
	}

	@Test
	public void testSanctionLoanAmount_LoanApproved() throws Exception {

		Optional<Long> response = Optional.of(new Long(705));
		when(creditScoreAPI.getCreditScore(any())).thenReturn(response);
		CreditDecisionResponse respOutput = creditDecisionService.sanctionLoanAmount(ssn, loanAmt, annualIncome);

		assertThat(respOutput.getLoanApproved(), is("Y"));

	}
	
	@Test
	public void testSanctionLoanAmount_LoanRejected() throws Exception {

		Optional<Long> response = Optional.of(new Long(600));
		when(creditScoreAPI.getCreditScore(any())).thenReturn(response);
		CreditDecisionResponse respOutput = creditDecisionService.sanctionLoanAmount(ssn, loanAmt, annualIncome);

		assertThat(respOutput.getLoanApproved(), is("N"));

	}

}
